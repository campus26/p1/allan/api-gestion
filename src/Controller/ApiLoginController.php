<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\DBAL\Exception;
use App\Repository\ExpenseRepository;
use App\Entity\Depense;
use App\Repository\CategoryRepository;
use App\Entity\Category;
use App\Repository\UserRepository;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class ApiLoginController extends AbstractController
{

    /**
     * @Route("/login/api", name="login_api", methods={"POST"})     
     * @param DepenseRepository $depenseRepository
     * @return Response
     */
    public function login(Request $request,UserRepository $userRepository,UserPasswordEncoderInterface $passwordEncoder) : Response
    {
        // Avec $request récupéré email et mdp
        $data = $request->toArray();
        $email = $data['email'];
        $mdp = $data['password'];

        // Tester si l'utilisateur existe
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->findOneBy(['email' =>$email]);

        if(is_null($user)){
            return $this->json('Utilisateur non valide');
        }
        // tester si le mot de passe est ok avec cet utilisateur
        elseif (!$passwordEncoder->isPasswordValid($user,$mdp)){
            return $this->json('Utilisateur non valide');
        }
        // Renvoyer le toke

        return $this->json($user->getApiToken());

    }
    
}

