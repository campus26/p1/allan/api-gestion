<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Depense;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DepenseFixture extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder){
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {

        $user = new User();
        $user->setEmail('oui@gmail.com');
        $user->setName('Nom');
        $user->setFirstname('Prenom');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'oui'
        ));

        $date = new \DateTime();
        $user->setApiToken(sha1($date->format('Y-m-d H:i:s')));

        $category = new Category();
        $category->setName('Restaurant');
        $category->setColor('red');
        $category->setIcon('icon');

        $depense = new Depense();
        $depense->setName('Ali baba');
        $depense->setDate(\DateTime::createFromFormat('d-m-Y', "01-09-2015"));
        $depense->setAmout('8');
        $depense->setPicture('picture');
        $depense->setUser($user);
        $depense->setCategory($category);

        $manager->persist($user);
        $manager->persist($category);
        $manager->persist($depense);

        $manager->flush();
    }
}
